You can consider donating if you want to support [@termux](https://github.com/termux) development, encourage us to bring more features/packages and provide a more reliable and faster repository mirrors, etc.

We are currently using github actions for building and uploading packages to a repository hosted by [FossHost](https://fosshost.org/). Thanks to this, the main infrastructure does not incur any costs for individual developers.

Donating to us will show your appreciation and help support the work (as in development or maintenance) of the various contributors. Even a small amount (for a cup of coffee) is greatly appreciated. Look at the git history and repository statistics ([`termux-packages`](https://github.com/termux/termux-packages/graphs/contributors), [`termux-app`](https://github.com/termux/termux-app/graphs/contributors)) to see all the people that have put time and effort into the packages and apps. Many of these people are outside contributors and not part of the [@termux team](https://github.com/orgs/termux/people), but should be supported as well. You can also check out [`CODEOWNERS`](https://github.com/termux/termux-packages/blob/master/CODEOWNERS) for the `termux-packages` if you want to support the maintenance of specific packages.

- Agnostic Apollo ([@agnostic-apollo](https://github.com/agnostic-apollo)) is today the main developer and maintainer of the android apps. See [Donations.md](https://github.com/agnostic-apollo/agnostic-apollo/blob/main/Donations.md) for how to donate.

-  Xeffyr ([@xeffyr](https://github.com/xeffyr)) makes contributions to all the repositories, and has set up the scripts for package builds and hosting. Donate ETH to `0x3CC8F61862e5c3Ad62A7eea0c3048853FA31af23` or TRX to `TLj9RmJYSoWYm1Ux7fP6xBwkViHCWwfUDM`.

- Grimler ([@grimler91](https://github.com/grimler91)) makes contributions to all the repositories. Donate ETH to `0x09D42e7518f5bF6eF173c536Ad8e87c973615A9D`, or through [liberapay](https://liberapay.com/grimler).

- Landfillbaby ([@landfillbaby](https://github.com/landfillbaby)) makes contributions to all the repositories, but mostly concerns herself with package updates. Donate through [liberapay](https://liberapay.com/landfillbaby) (preferred) or [Ko-fi](https://ko-fi.com/landfillbaby).

- Fornwall ([@fornwall](https://github.com/fornwall)) is the creator of the [@termux](https://github.com/termux) project and was for several years the sole termux developer and created the apps and ported many packages, but is currently inactive. You can donate through https://paypal.me/fornwall, https://www.patreon.com/termux, or by buying the (now [deprecated](https://github.com/termux/termux-app#google-play-store-deprecated) and basically non-working) termux addon apps on Google play ([Termux:API](https://play.google.com/store/apps/details?id=com.termux.api), [Termux:Float](https://play.google.com/store/apps/details?id=com.termux.window), [Termux:Widget](https://play.google.com/store/apps/details?id=com.termux.widget), [Termux:Styling](https://play.google.com/store/apps/details?id=com.termux.styling), [Termux:Tasker](https://play.google.com/store/apps/details?id=com.termux.tasker), [Termux:Boot](https://play.google.com/store/apps/details?id=com.termux.boot)).
##

*Note for [@termux](https://github.com/termux) maintainers: Feel free to add/update/remove your donation links.*
